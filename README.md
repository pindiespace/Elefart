Elefart
========

An elevator chase where blasts with gas keep you from reaching the top.

Programmed with:
- HTML5 standards-based markup
- CSS/CSS3
- JavaScript polyfills to increas compatibility
- Fluid (rather than responsive) design techniques
- No frameworks

Versions
=========

  0.1 - Initial boilerplate
  
